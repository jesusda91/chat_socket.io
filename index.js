var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var nick= [];


app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg, nick){
    io.emit('chat message', msg, nick);
  });

  socket.on('writing', function (nick) {
  	nick= nick;
  	io.emit('writing', true, nick);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});